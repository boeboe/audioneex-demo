APP_ABI := armeabi armeabi-v7a
APP_OPTIM := release
APP_STL := gnustl_shared
APP_CPPFLAGS += -fexceptions
APP_PLATFORM := android-9
NDK_TOOLCHAIN_VERSION := 4.8
